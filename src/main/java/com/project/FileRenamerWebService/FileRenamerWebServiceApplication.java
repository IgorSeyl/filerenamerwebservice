package com.project.FileRenamerWebService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class FileRenamerWebServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileRenamerWebServiceApplication.class, args);
    }

}
