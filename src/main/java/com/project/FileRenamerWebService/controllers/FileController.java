package com.project.FileRenamerWebService.controllers;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.zeroturnaround.zip.ZipUtil;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

@Controller
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ResponseBody
    public void uploadFiles(
            HttpServletResponse response,
            @RequestParam("excel") MultipartFile excel,
            @RequestParam(name = "extension", defaultValue = "false") boolean extension,
            @RequestParam("files") MultipartFile... files)
            throws IOException {

        // вылечить фишку с использованием >1 клиентом - делать темп папку с именем потока

        String loadingDir = System.getProperty("user.dir") + File.separator + "loaded" + File.separator;
        String excelPath = loadingDir + excel.getOriginalFilename();
        String filesPath = loadingDir + "renamed files";

        loadFiles(loadingDir, excel);
        loadFiles(filesPath, files);
        rename(extension, excelPath, filesPath);
        createZip(filesPath, loadingDir, "renamed files");
        responseZip(response, loadingDir + "renamed files.zip");
        deleteFiles(loadingDir, filesPath);
    }

    private void loadFiles(String path, MultipartFile... files) throws IOException {

        for (MultipartFile file : files) {

            byte[] bytes = file.getBytes();
            String name = file.getOriginalFilename();

            File uploadedFile = new File(path + File.separator + name);
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
            stream.write(bytes);
            stream.flush();
            stream.close();

            logger.info("uploaded: " + uploadedFile.getAbsolutePath());
        }
    }

    private void rename(boolean extension, String excelPath, String filesPath) throws IOException {

        XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(excelPath));
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row = null;

        File[] files = new File(filesPath).listFiles();

        for (int i = 0; i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            row.getCell(0).setCellType(CellType.STRING);
            row.getCell(1).setCellType(CellType.STRING);
        }

        assert files != null;
        for (File file : files) {

            for (int i = 0; i <= sheet.getLastRowNum(); i++) {

                row = sheet.getRow(i);
                String fileFullName = file.getName();

                int fileExtensionIndex = fileFullName.lastIndexOf(".");
                String fileExtension = fileFullName.substring(fileExtensionIndex);
                String fileName = fileFullName.replace(fileFullName.substring(fileExtensionIndex), "");

                String prevNameExcel = row.getCell(0).getStringCellValue();
                String newNameExcel = row.getCell(1).getStringCellValue();

                if (extension){

                    if (fileFullName.equals(prevNameExcel)) {
                        file.renameTo(new File(filesPath + File.separator + newNameExcel));
                        break;
                    }
                } else {

                    if (fileName.equals(prevNameExcel)) {

                        file.renameTo(new File(filesPath + File.separator + newNameExcel + fileExtension));
                        break;
                    }
                }
            }
        }
    }

    private void createZip(String filesPath, String destPath, String zipName) {

        ZipUtil.pack(
                new File(filesPath),
                new File(destPath + zipName + ".zip")
                , name -> zipName + File.separator + name
        );
    }

    private void responseZip(HttpServletResponse response, String zipPath) throws IOException {

        File file = new File(zipPath);

        response.setContentType("application/zip"); // ???
        response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName() + "\"");
        BufferedInputStream inStrem = new BufferedInputStream(new FileInputStream(file));
        BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

        byte[] buffer = new byte[1024];
        int bytesRead = 0;
        while ((bytesRead = inStrem.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
        outStream.flush();
        inStrem.close();
    }

    private void deleteFiles(String excelPath, String filesPath) {

        File[] files = new File(excelPath).listFiles();
        for (File file : files) if (!file.isDirectory()) file.delete();

        files = new File(filesPath).listFiles();
        for (File file : files) file.delete();
    }
}
